import random
from typing import List


class Board:
    size: int
    board: List[int]

    def __init__(self, size):
        self.size = size
        self.board = [random.randint(0, size - 1) for _ in range(size)]

    def evaluate(self):
        h = 0
        for i in range(self.size):
            for j in range(i + 1, self.size):
                if self.board[i] == self.board[j] or self.board[i] + (j - i) == self.board[j] or \
                        self.board[i] - (j - i) == self.board[j]:
                    h += 1
        return h

    def __str__(self):
        ret = ''
        ret += f'current collides: {self.evaluate()}\n'
        for i in range(self.size):
            for j in range(self.size):
                if i == self.board[j]:
                    ret += '*' if (i + j) % 2 == 0 else "\033[97;43m*\033[m"
                else:
                    ret += ' ' if (i + j) % 2 == 0 else "\033[30;43m \033[m"
            ret += '\n'
        return ret
