from math import exp
from random import random

from lib.board import Board


class SimulatedAnnealing:
    t: int
    chess: Board
    result: bool

    def __init__(self, t: int, chess: Board):
        self.t = t
        self.chess = chess
        self.result = self.search()

    def search(self):
        new_state = False
        while self.t > 0 or new_state:
            new_state = False
            for i in range(self.chess.size):
                for j in range(self.chess.size):
                    current_place = self.chess.board[i]
                    if j == current_place:
                        continue
                    current_heuristic = self.chess.evaluate()
                    if current_heuristic == 0:
                        return True
                    self.chess.board[i] = j
                    new_heuristic = self.chess.evaluate()
                    if new_heuristic < current_heuristic:
                        new_state = True
                        break
                    delta = current_heuristic - new_heuristic
                    if self.t > 0 > delta and random() < exp(delta / self.t):
                        new_state = True
                        break
                    self.chess.board[i] = current_place
                if new_state:
                    break
            self.t -= 0.001
        return False

    def __str__(self):
        ret = ''
        if self.result:
            ret += 'Successful!\n'
        else:
            ret += 'Failed!\n'
        ret += f"Final temperature: {self.t}\n"
        ret += f'{self.chess}'
        return ret
