from lib.board import Board
from lib.simulated_annealing import SimulatedAnnealing
from colorama import init

init()

if __name__ == '__main__':
    print("Enter the size of the board:")
    chess = Board(int(input()))
    print(chess)
    solver = SimulatedAnnealing(3, chess)
    print(solver)
