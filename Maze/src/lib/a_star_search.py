from queue import PriorityQueue
from typing import List

from lib.maze_world import MazeWorld


class AStarSearch:
    length: int
    width: int
    start_x: int
    start_y: int
    goal_x: int
    goal_y: int
    maze: MazeWorld
    par: List[List[int]]
    costs: List[List[int]]
    inf = 100

    def __init__(self, maze: MazeWorld, start: (int, int), goal: (int, int)):
        self.length = maze.length
        self.width = maze.width
        self.maze = maze
        self.start_x, self.start_y = start
        self.start_x -= 1
        self.start_y -= 1
        self.goal_x, self.goal_y = goal
        self.goal_x -= 1
        self.goal_y -= 1
        self.par = [[0 for _ in range(self.length)] for _ in range(self.width)]
        self.costs = [[self.inf for _ in range(self.length)] for _ in range(self.width)]
        self.search()

    def search(self):
        self.costs[self.start_y][self.start_x] = 0
        queue = PriorityQueue()
        queue.put((self.heuristic(self.start_x, self.start_y, self.goal_x, self.goal_y), self.start_x, self.start_y))
        while not queue.empty():
            (_, x, y) = queue.get()
            if x is self.goal_x and y is self.goal_y:
                break
            for (next_x, next_y, action) in self.maze.neighbors(x, y):
                new_const = self.costs[y][x] + 1
                if new_const < self.costs[next_y][next_x]:
                    self.costs[next_y][next_x] = new_const
                    queue.put((new_const + self.heuristic(next_x, next_y, self.goal_x, self.goal_y), next_x, next_y))
                    self.par[next_y][next_x] = action

    @staticmethod
    def heuristic(x: int, y: int, xx: int, yy: int) -> int:
        return abs(x - xx) + abs(y - yy)  # Manhattan distance heuristic

    def get_path(self) -> [(int, int)]:
        ret = []
        x = self.goal_x
        y = self.goal_y
        while x is not self.start_x or y is not self.start_y:
            ret.append((x, y))
            action = self.par[y][x]
            if action & 0b0101 > 0:
                x += 1 if action & 0b0100 else -1
            if action & 0b1010 > 0:
                y += 1 if action & 0b0010 else -1
        ret.append((self.start_x, self.start_y))
        ret.reverse()
        return ret

    def __str__(self):
        ret = ''
        path = self.get_path()
        for item in path:
            ret += f'({item[0]} {item[1]})-> '
        ret += 'Done!\n'
        ret += f"Final cost: {self.costs[self.goal_y][self.goal_x]}\n"
        for i in range(self.length):
            ret += ' _'
        ret += '\n'
        for i in range(self.width):
            ret += '│'
            for j in range(self.length):
                colored = True if (j, i) in path else False
                if self.maze.map[i][j] & 0b0010 > 0:
                    ret += "\033[30;46m_\033[m" if colored else '_'
                else:
                    ret += "\033[30;46m \033[m" if colored else ' '
                if self.maze.map[i][j] & 0b0100 > 0:
                    ret += '│'
                else:
                    ret += "\033[30;46m \033[m" if colored and (j + 1, i) in path else ' '
            ret += '\n'
        return ret
