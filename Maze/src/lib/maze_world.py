from typing import List


class MazeWorld:
    length: int
    width: int
    map: List[List[int]]

    def __init__(self, input_path: str):
        with open(input_path) as f:
            lines = f.readlines()
        self.length, self.width = map(int, lines[0].split())
        self.map = [[0 for _ in range(self.length)] for _ in range(self.width)]
        i = 2
        while 'horizontal:' not in lines[i]:
            x, y = map(int, lines[i].split())
            self.set_wall(x, y, True)
            i += 1
        i += 1
        while i < len(lines):
            x, y = map(int, lines[i].split())
            self.set_wall(x, y, False)
            i += 1
        for i in range(self.length):
            self.map[-1][i] |= 0b0010
            self.map[0][i] |= 0b1000
        for i in range(self.width):
            self.map[i][-1] |= 0b0100
            self.map[i][0] |= 0b0001

    def set_wall(self, x, y, direction):  # true means vertical
        if direction:
            self.map[y - 1][x - 1] |= 0b0100
            self.map[y - 1][x] |= 0b0001
        else:
            self.map[y - 1][x - 1] |= 0b0010
            self.map[y][x - 1] |= 0b1000

    def neighbors(self, x, y):  # -> List[(int, int, int)]:
        ret = []
        if self.map[y][x] | 0b0111 != 15:
            ret.append((x, y - 1, 0b0010))
        if self.map[y][x] | 0b1011 != 15:
            ret.append((x + 1, y, 0b0001))
        if self.map[y][x] | 0b1101 != 15:
            ret.append((x, y + 1, 0b1000))
        if self.map[y][x] | 0b1110 != 15:
            ret.append((x - 1, y, 0b0100))
        return ret

    def __str__(self):
        ret = ''
        for i in range(self.length):
            ret += ' _'
        ret += '\n'
        for i in self.map:
            ret += '│'
            for j in i:
                if j & 0b0010 > 0:
                    ret += '_'  # '_' '_' '▁' '_'
                else:
                    ret += ' '
                if j & 0b0100 > 0:
                    ret += '│'
                else:
                    ret += ' '
            ret += '\n'
        return ret
