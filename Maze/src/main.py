from lib.a_star_search import AStarSearch
from lib.maze_world import MazeWorld
from colorama import init

init()

if __name__ == '__main__':
    maze = MazeWorld('./map.txt')
    print(maze)
    print("Enter your starting point:")
    start = tuple(map(int, input().split()))
    print("Enter your goal:")
    goal = tuple(map(int, input().split()))
    search = AStarSearch(maze, start, goal)
    print(search)
