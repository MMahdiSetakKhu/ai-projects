from lib.game import Game


class MinimaxAgent:
    game: Game

    def __init__(self, game: Game):
        self.game = game

    def best_action(self, player: int) -> (int, int):
        if player == 1:
            ret = self.max_value(-2, 2)
        else:
            ret = self.min_value(-2, 2)
        return ret[1]

    def max_value(self, alfa, beta) -> (int, (int, int)):
        result = self.game.evaluation()
        best_action = (-1, -1)
        if result is not None:
            return result, best_action
        value = -2
        for action in self.game.get_actions():
            self.game.board[action[0]][action[1]] = 1
            (v, _) = self.min_value(alfa, beta)
            self.game.board[action[0]][action[1]] = 0
            if v > value:
                value = v
                best_action = action
            if value >= beta:
                return value, best_action
            alfa = max(value, alfa)
        return value, best_action

    def min_value(self, alfa, beta) -> (int, (int, int)):
        result = self.game.evaluation()
        best_action = (-1, -1)
        if result is not None:
            return result, best_action
        value = 2
        for action in self.game.get_actions():
            self.game.board[action[0]][action[1]] = -1
            (v, _) = self.max_value(alfa, beta)
            self.game.board[action[0]][action[1]] = 0
            if v < value:
                value = v
                best_action = action
            if value <= alfa:
                return value, best_action
            beta = min(value, beta)
        return value, best_action
