from random import random
from tkinter import Tk, Button, Label, OptionMenu, StringVar

from lib.game import Game
from lib.minimax_agent import MinimaxAgent


class TicTacToe:
    game: Game
    modes = ["Human vs Human", "Human vs AI"]
    mode: StringVar
    player = 'O'

    def __init__(self):
        self.game = Game()
        window = Tk()
        window.title("Tic Tac Toe")
        self.tiles = []
        for i in range(3):
            self.tiles.append([])
            for j in range(3):
                self.tiles[i].append(Button(text='', font=('normal', 60, 'normal'), width=3, height=1,
                                            command=lambda r=i, c=j: self.action_handler(r, c)))
                self.tiles[i][j].grid(row=i, column=j)
        self.status = Label(text="It's O's turn", font=('normal', 22, 'bold'))
        self.status.grid(row=3, column=1)
        reset_btn = Button(text='restart', font=('Courier', 18, 'normal'), fg='red', command=self.restart)
        reset_btn.grid(row=4, column=2)
        self.mode = StringVar()
        self.mode.set(self.modes[1])
        menu = OptionMenu(window, self.mode, *self.modes)
        menu.grid(row=4, column=0)
        self.random_start()
        window.mainloop()

    def action_handler(self, i, j):
        if self.player == 'O' or self.player == 'X':
            self.apply_action(i, j)
        if not self.check_end() and self.mode.get() == "Human vs AI":
            action = MinimaxAgent(self.game).best_action(1 if self.player == 'O' else -1)
            self.apply_action(action[0], action[1])
            self.check_end()

    def check_end(self):
        result = self.game.evaluation()
        if result is not None:
            self.status.config(text="O won!" if result == 1 else ("X won!" if result == -1 else "Draw!"))
            self.player = "Done!"
        return result is not None

    def apply_action(self, i, j):
        self.tiles[i][j].config(text=self.player, state="disabled")
        self.game.board[i][j] = 1 if self.player == 'O' else -1
        self.player = 'X' if self.player == 'O' else 'O'
        self.status.config(text=f"It's {self.player}'s turn")

    def random_start(self):
        chance = random()
        if chance >= 0.5:
            action = MinimaxAgent(self.game).best_action(1 if self.player == 'O' else -1)
            self.apply_action(action[0], action[1])

    def restart(self):
        self.player = 'O'
        self.game = Game()
        for i in range(3):
            for j in range(3):
                self.tiles[i][j].config(text='', state="active")
        self.status.config(text=f"It's {self.player}'s turn")
        if self.mode.get() == "Human vs AI":
            self.random_start()
