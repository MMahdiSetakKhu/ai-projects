from typing import List


class Game:
    board: List[List[int]]  # 1 for O, 0 for empty and -1 for X

    def __init__(self):
        self.board = [[0 for _ in range(3)] for _ in range(3)]

    def evaluation(self):
        for i in range(3):
            if self.board[i][0] == self.board[i][1] == self.board[i][2]:
                if self.board[i][0] != 0:
                    return self.board[i][0]
            if self.board[0][i] == self.board[1][i] == self.board[2][i]:
                if self.board[0][i] != 0:
                    return self.board[0][i]
        if self.board[0][0] == self.board[1][1] == self.board[2][2]:
            if self.board[0][0] != 0:
                return self.board[0][0]
        if self.board[0][2] == self.board[1][1] == self.board[2][0]:
            if self.board[0][2] != 0:
                return self.board[0][2]
        for i in range(3):
            for j in range(3):
                if self.board[i][j] == 0:
                    return None
        return 0

    def get_actions(self):
        ret = []
        for i in range(3):
            for j in range(3):
                if self.board[i][j] == 0:
                    ret.append((i, j))
        return ret
